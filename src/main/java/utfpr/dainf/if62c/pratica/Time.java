package utfpr.dainf.if62c.pratica;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class Time {
    private final HashMap<String, Jogador> jogadores = new HashMap<>();

    public Time() {
    }

    public HashMap<String, Jogador> getJogadores() {
        return jogadores;
    }
    
    public void addJogador(String posicao, Jogador jogador) {
        jogadores.put(posicao, jogador);
    }
    
    public List<Jogador> ordena(JogadorComparator comparator) {
        List<Jogador> list = new ArrayList<>();
        list.addAll(jogadores.values());
        list.sort(comparator);
        return list;
    }
    
}
